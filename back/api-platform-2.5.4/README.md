# API platform #

## INSTALLATION ##

Download the file directory of API platform from the below link\
https://github.com/api-platform/api-platform/releases/tag/v2.5.4 \
To avoid the permission issues recomanded to download tar.gz directory

## Docker services ##

To start the docker services
```
$ docker-compose pull
$ docker-compose up -d
```

## Database schema ##

To create the database and schema
```
$ bin/console doctrine:database:create
$ bin/console doctrine:schema:create
```

## JWT Token ##

Include the JWT token using the below command
```
composer req "lexik/jwt-authentication-bundle"

```


