import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './modules/material.module';

import { ToRepeatDirective } from './directives/to-repeat.directive';

import { AppComponent } from './app.component';

import { LandingPageComponent } from './components/landing-page/landing-page.component';

import { MenuComponent } from './components/common/menu/menu.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { PlateformeComponent } from './components/plateforme/plateforme.component';
import { DashboardComponent } from './components/plateforme/dashboard/dashboard.component';
import { MissionsComponent } from './components/plateforme/missions/missions.component';
import { ComptaComponent } from './components/plateforme/compta/compta.component';
import { AppRoutingModule } from './modules/app-routing.module';
import { ConnexionComponent } from './components/plateforme/connexion/connexion.component';
import { BadgeComponent } from './components/plateforme/dashboard/badge/badge.component';
import { CalendarComponent } from './components/plateforme/dashboard/calendar/calendar.component';
import { ChiffresComponent } from './components/plateforme/dashboard/chiffres/chiffres.component';
import { FilActuComponent } from './components/plateforme/dashboard/fil-actu/fil-actu.component';
import { ProfilComponent } from './components/plateforme/profil/profil.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule
  ],
  declarations: [
    ToRepeatDirective,
    AppComponent,
    MenuComponent,
    LandingPageComponent,
    HeaderComponent,
    FooterComponent,
    PlateformeComponent,
    DashboardComponent,
    MissionsComponent,
    ComptaComponent,
    ConnexionComponent,
    BadgeComponent,
    CalendarComponent,
    ChiffresComponent,
    FilActuComponent,
    ProfilComponent,
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
