import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatAutocompleteModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule,
  MatSelectModule, MatSlideToggleModule, MatMenuModule, MatToolbarModule, MatCardModule, MatExpansionModule,
  MatGridListModule, MatTabsModule, MatTreeModule, MatButtonModule, MatBadgeModule, MatChipsModule, MatIconModule,
  MatProgressBarModule, MatRipple, MatRippleModule, MatDialogModule, MatSnackBarModule, MatTooltipModule,
  MatTableModule,
  MatDividerModule,
  MatRadioModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter'



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatAutocompleteModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule,
    MatSelectModule, MatSlideToggleModule, MatMenuModule, MatToolbarModule, MatCardModule, MatExpansionModule,
    MatGridListModule, MatTabsModule, MatTreeModule, MatButtonModule, MatBadgeModule, MatChipsModule, MatIconModule,
    MatProgressBarModule, MatRippleModule, MatDialogModule, MatSnackBarModule, MatTooltipModule, MatTableModule, MatMomentDateModule,
    MatDividerModule, MatRadioModule
  ],
  exports: [
    MatAutocompleteModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatInputModule,
    MatSelectModule, MatSlideToggleModule, MatMenuModule, MatToolbarModule, MatCardModule, MatExpansionModule,
    MatGridListModule, MatTabsModule, MatTreeModule, MatButtonModule, MatBadgeModule, MatChipsModule, MatIconModule,
    MatProgressBarModule, MatRippleModule, MatDialogModule, MatSnackBarModule, MatTooltipModule, MatTableModule, MatMomentDateModule,
    MatDividerModule, MatRadioModule
  ]
})
export class MaterialModule { }
