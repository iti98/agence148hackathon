import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from '../guards/authentication.guard';

import { LandingPageComponent } from '../components/landing-page/landing-page.component';
import { PlateformeComponent } from '../components/plateforme/plateforme.component';
import { DashboardComponent } from '../components/plateforme/dashboard/dashboard.component';
import { MissionsComponent } from '../components/plateforme/missions/missions.component';
import { ComptaComponent } from '../components/plateforme/compta/compta.component';
import { ConnexionComponent } from '../components/plateforme/connexion/connexion.component';
import { ProfilComponent } from '../components/plateforme/profil/profil.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  {
    path: 'plateforme',
    component: PlateformeComponent,
    canActivate: [AuthenticationGuard],
    children: [
      { path: 'login', component: ConnexionComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'missions', component: MissionsComponent },
      { path: 'comptabilite', component: ComptaComponent },
      { path: 'profil', component: ProfilComponent },
      { path: '**', redirectTo: 'dashboard' }
    ]
  },
  { path: '**', redirectTo: '' }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
