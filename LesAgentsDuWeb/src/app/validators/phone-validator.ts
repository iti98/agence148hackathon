import { AbstractControl } from '@angular/forms'

export function phoneNumberValidator(control: AbstractControl): { [key: string]: any } | null {
  const valid1 = /^\d+$/.test(control.value);
  const valid2 = /^[0-9]{10}$/.test(control.value);
  const valid3 = /^\d{2}\s\d{2}\s\d{2}\s\d{2}\s\d{2}$/.test(control.value);
  const valid4 = /^\+\d{11,14}$/.test(control.value);
  return valid1 && (valid2 || valid3 || valid4) ? null : { invalidNumber: { valid: false, value: control.value } }
}