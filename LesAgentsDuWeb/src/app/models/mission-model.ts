import { Moment } from 'moment';

export class MissionModel {
    constructor(
        public id?: number,
        public tarif?: TarifModel,
        public intitule?: string,
        public time?: string,
        public cp?: string,
        public besoins?: string
    ) { }
}

export class TarifModel {
    constructor(
        public cout?: number,
        public devise?: string,
        public temps?: string,
        public coutTotal?: number
    ) { }
}
