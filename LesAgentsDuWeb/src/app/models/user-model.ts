import { MissionModel } from './mission-model';
import { BadgeModel } from './badge-model';

export class UserModel {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public email?: string,
    public tel?: string,
    public mission?: MissionModel,
    public badges?: BadgeModel[],
    public historiqueMissions?: MissionModel[],
    public niveau?: number,
    public xp?: number,
    public savings?: number,
    public adress?: string,
    public codePostale?: string,
    public ville?: string,
    public metier?: string,
    public description?: string,
    public picUrl?: string
  ) { }
}
