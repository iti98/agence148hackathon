import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { phoneNumberValidator } from 'src/app/validators/phone-validator';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(
    private fb: FormBuilder
  ) { }

  public becameAgentForm: FormGroup;

  ngOnInit() {
    this.formInit();
  }

  private formInit() {
    this.becameAgentForm = this.fb.group({
      name: new FormControl('', [Validators.required]),
      prename: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      tel: new FormControl('', [Validators.required, phoneNumberValidator]),
      message: new FormControl('', [Validators.required, Validators.maxLength(1064)])
    })
  }

  public onSubmit() {
    console.log('Values', this.becameAgentForm.value);
  }

}
