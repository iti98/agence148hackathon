import { Component, OnInit } from '@angular/core';
import { MissionModel, TarifModel } from 'src/app/models/mission-model';
import { UserModel } from 'src/app/models/user-model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.css']
})
export class MissionsComponent implements OnInit {

  user: UserModel = new UserModel();
  private userSub: Subscription;

  projList = [
    new MissionModel(45, new TarifModel(75, 'euro', 'demi-journée', 1600), 'Refonte du site de l\'epicier d\'a coté.', '10 jours', 'Jackie Chan', '1 Developpeur, 1 Designer'),
    new MissionModel(50, new TarifModel(160, 'euro', 'demi-journée', 7200), 'Refonte du site de Liddle.', '30 jours', 'Jean pierre', '2 Developpeur, 1 Designer'),
    new MissionModel(61, new TarifModel(80, 'dollars', 'demi-journée', 4500), 'Refonte du site de Wallmart.', '1 mois', 'Jackie Chan', '1 Developpeur, 1 Designer'),
  ];
  constructor(
    private userServ: UserService
  ) { }

  ngOnInit() {
    this.userSub = this.userServ.user.subscribe(data => {
      this.user = data;
    });
    this.userServ.emitUser();
  }

}
