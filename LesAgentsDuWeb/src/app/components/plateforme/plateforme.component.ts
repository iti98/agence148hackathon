import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/app/services/route.service';

@Component({
  selector: 'app-plateforme',
  templateUrl: './plateforme.component.html',
  styleUrls: ['./plateforme.component.css']
})
export class PlateformeComponent implements OnInit {

  hasNotification: boolean = false;

  constructor(
    private routeServ: RouteService
  ) { }

  ngOnInit() {
  }

  public onGoToSettings() {
    console.log('Clic sur settings.');
    this.hasNotification = !this.hasNotification;
  }

  public getProfilePath() {
    return this.routeServ.getProfilPath();
  }
}
