import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserModel } from 'src/app/models/user-model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit, OnDestroy {

  profileForm: FormGroup;
  user: UserModel = new UserModel();
  private userSub: Subscription;

  constructor(
    private fb: FormBuilder,
    private userServ: UserService
  ) { }

  ngOnInit() {
    this.userSub = this.userServ.user.subscribe(data => {
      this.user = data;
    });
    this.userServ.emitUser();
    this.initForm();
  }
  ngOnDestroy() {
    this.userSub.unsubscribe();
    this.onSubmit();
  }

  private initForm() {
    this.profileForm = this.fb.group({
      name: new FormControl((this.user ? this.user.name ? this.user.name : '' : ''), Validators.required),
      surname: new FormControl((this.user ? this.user.surname ? this.user.surname : '' : ''), Validators.required),
      email: new FormControl((this.user ? this.user.email ? this.user.email : '' : ''), Validators.required),
      tel: new FormControl((this.user ? this.user.tel ? this.user.tel : '' : ''), Validators.required),
      adress: new FormControl((this.user ? this.user.adress ? this.user.adress : '' : ''), Validators.required),
      codePostale: new FormControl((this.user ? this.user.codePostale ? this.user.codePostale : '' : ''), Validators.required),
      ville: new FormControl((this.user ? this.user.ville ? this.user.ville : '' : ''), Validators.required),
      metier: new FormControl((this.user ? this.user.metier ? this.user.metier : '' : ''), Validators.required),
      description: new FormControl((this.user ? this.user.description ? this.user.description : '' : ''), Validators.maxLength(1064))
    });
  }

  public getProfilePic() {
    if (this.user && this.user.picUrl) {
      return this.user.picUrl;
    }
    return '../../../../assets/noProfilePic.png';
  }

  onSubmit() {
    if (this.profileForm.valid) {
      const usr: UserModel = new UserModel(
        this.user.id,
        this.profileForm.get('name').value,
        this.profileForm.get('surname').value,
        this.profileForm.get('email').value,
        this.profileForm.get('tel').value,
        this.user.mission,
        this.user.badges,
        this.user.historiqueMissions,
        this.user.niveau,
        this.user.xp,
        this.user.savings,
        this.profileForm.get('adress').value,
        this.profileForm.get('codePostale').value,
        this.profileForm.get('ville').value,
        this.profileForm.get('metier').value,
        this.profileForm.get('description').value,
        this.profileForm.get('picUrl').value
      )
      this.userServ.modifyUser(this.profileForm.value);
    }
  }
}
