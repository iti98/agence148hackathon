import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from 'src/app/models/user-model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.css']
})
export class BadgeComponent implements OnInit {

  public user: UserModel = new UserModel();
  private userSub: Subscription;

  constructor(
    private userServ: UserService
  ) { }

  ngOnInit() {
    this.userSub = this.userServ.user.subscribe(data => {
      this.user = data; 
    })
  }

  public getFirstBadge(): string {
    // TODO deteminate the badge with the level
    let path;
    if (this.user && this.user.niveau) {
      if (this.user.niveau === 0) {
        path = '../../../../../assets/badges/nobadge.png';
      } else if (this.user.niveau === 1) {
        path = '../../../../../assets/badges/petitagent.png';
      } else if (this.user.niveau === 2) {
        path = '../../../../../assets/badges/agentconfirme.png';
      } else if (this.user.niveau === 3) {
        path = '../../../../../assets/badges/superagent.png';
      }
    }
    
    return path;
  }
  public getSeccondBadge() {
    // TODO deteminate the badge with the level
    let path;
    if (this.user && this.user.niveau) {
      if (this.user && this.user.niveau) {
        path = '../../../../../assets/badges/petitagent.png';
      } else if (this.user.niveau === 1) {
        path = '../../../../../assets/badges/agentconfirme.png';
      } else if (this.user.niveau >= 2) {
        path = '../../../../../assets/badges/superagent.png';
      }
    }
    return path;
  }

}
