import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/app/services/route.service';

@Component({
  selector: 'app-fil-actu',
  templateUrl: './fil-actu.component.html',
  styleUrls: ['./fil-actu.component.css']
})
export class FilActuComponent implements OnInit {

  hasMessage = true

  user = {name: 'kikoooo'}

  list = [{path: 'bouh', text: 'hey'}, {path: 'bououououo', text: 'hey heyheeeeeyyy '}];

  constructor(private routeServ: RouteService) { }

  ngOnInit() {
  }

  getActuList() {
    const toReturn = [];
    if (this.hasNewMessage()) {
      toReturn.push({path: this.routeServ.getMessagePath(), text: 'Vous avez un nouveau message.'})
    } 
    if (!this.isProfilComplete) {
      toReturn.push({path: this.routeServ.getProfilPath(), text: 'Complétez votre profile !'})
    }
    if (this.hasMission) {
      toReturn.push({path: this.routeServ.getMissionPath(), text: 'Vérifiez votre mission'});
    } else {
      toReturn.push({path: this.routeServ.getMissionPath(), text: 'Des missions vous attendent !'})
    }
    return toReturn;
  }

  private hasNewMessage(): boolean {
    // TODO verifier si l'utilisateur a des nouveaux messages 
    return true;
  }

  private isProfilComplete(): boolean {
    // TODO un test pour voir si cle profil est complété
    return false;
  }

  private hasMission(): boolean {
    // TODO un test pour vérifier si l'utilisateur a une mission
    return true;
  }

}
