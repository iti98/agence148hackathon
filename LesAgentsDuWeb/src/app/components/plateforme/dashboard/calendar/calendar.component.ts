import { Component, OnInit, ViewChild } from '@angular/core';
import { MatCalendar } from '@angular/material';
import { Moment } from 'moment';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  @ViewChild('calendar',{static: true}) calendar: MatCalendar<Moment>;
  
  constructor() { }

  ngOnInit() {
  }

  selectedDate: Moment;
}
