import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/user-model';
import { UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chiffres',
  templateUrl: './chiffres.component.html',
  styleUrls: ['./chiffres.component.css']
})
export class ChiffresComponent implements OnInit {

  public user: UserModel = new UserModel();
  private userSub: Subscription;

  constructor(private userServ: UserService) { }

  ngOnInit() {
    this.userSub = this.userServ.user.subscribe(data => {
      this.user = data;
    });
    this.userServ.emitUser();
  }



}
