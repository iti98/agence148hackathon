import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public menuTree = [
    { display: 'Acceuil', path: 'dashboard' },
    { display: 'Missions', path: 'missions' },
    { display: 'Comptabilité', path: 'comptabilite' } 
  ];

  constructor() { }

  ngOnInit() {
  }

}
