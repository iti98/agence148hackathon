import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { UserModel } from '../models/user-model';
import { UserService } from './user.service';
import { MissionModel, TarifModel } from '../models/mission-model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuthValue: boolean = false;
  isAuth = new Subject<boolean>();
  authMessageValue: string = '';
  authMessage = new Subject<string>();

  constructor(private userServ: UserService) { }

  public login(form) {
    const logincriteria = {
      username: form.user,
      password: form.pwd
    };
    // TODO requette au back pour s'autentifier
    setTimeout(() => {
      if (logincriteria.username === 'hpatrick' && logincriteria.password === 'azerty') {
        this.isAuthValue = true;
        this.userServ.setUser(new UserModel(32154, 'Patrick', 'Hioul', 'hioul.patrick@yahoo.fr', '0745454545', null, [], [], 0, 0, 0));
      } else if (logincriteria.username === 'rjackie' && logincriteria.password === 'azerty') {
        this.isAuthValue = true;
        this.userServ.setUser(new UserModel(2154, 'Jackie', 'Rohan', 'rohanj@free.fr', '0751155115', null, [], [new MissionModel(45, new TarifModel(75, 'euro', 'demi-journée', 1600), 'Refonte du site de l\'epicier d\'a coté.', '10 jours', 'Jackie Chan', '1 Developpeur, 1 Designer')], 1, 12, 2200));
      } else {
        this.authMessageValue = 'Identifiant ou mot de passe incorect.';
      }
      this.emitAuthMessage();
      this.emitIsAuth();
      this.userServ.emitUser();
    }, 1000)
  }

  public emitIsAuth() {
    this.isAuth.next(this.isAuthValue);
  }
  public emitAuthMessage() {
    this.authMessage.next(this.authMessageValue);
  }

}
