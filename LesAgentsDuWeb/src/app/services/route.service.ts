import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  constructor() { }

  public getMissionPath(): string {
    return this.getPlatformPath() + '/missions';
  }

  public getMessagePath(): string {
    return this.getPlatformPath() + '/message';
  }

  public getDashboardPath(): string {
    return this.getPlatformPath() + '/dashboard';
  }

  public getProfilPath(): string {
    return this.getPlatformPath() + '/profil';
  }

  public getComptabilitePath(): string {
    return this.getPlatformPath() + '/comptabilite';
  }

  public getLoginPath(): string {
    return this.getPlatformPath() + '/login';
  }

  private getPlatformPath(): string {
    return '/plateforme';
  }
}
