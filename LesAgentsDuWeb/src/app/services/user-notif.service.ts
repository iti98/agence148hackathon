import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserNotifService {

  constructor() { }

  private notifList: any[] = [];
  public notifListSubject = new Subject<any[]>();

  public getNotifListFromUser(uid: number) {
    
  }

  public emitNotifList() {
    this.notifListSubject.next(this.notifList);
  }
}
