import { Injectable } from '@angular/core';
import { UserModel } from '../models/user-model';
import { Subject } from 'rxjs';
import { MissionModel, TarifModel } from '../models/mission-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userValue: UserModel = new UserModel(2154, 'Jackie', 'Rohan', 'rohanj@free.fr', '0751155115', null, [], [new MissionModel(45, new TarifModel(75, 'euro', 'demi-journée', 1600), 'Refonte du site de l\'epicier d\'a coté.', '10 jours', 'Jackie Chan', '1 Developpeur, 1 Designer')], 1, 12, 2200);
  public user = new Subject<UserModel>();

  constructor() { }

  public getUser(): UserModel {
    return this.userValue;
  }

  public setUser(user: UserModel): void {
    this.userValue = user;
    this.emitUser();
  }

  public modifyUser(user) {
    this.setUser(user);
  }

  public emitUser() {
    this.user.next(this.userValue);
  }
}
