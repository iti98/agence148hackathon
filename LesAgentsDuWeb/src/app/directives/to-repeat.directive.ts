import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[toRepeat]'
})
export class ToRepeatDirective {

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { }

  @Input('toRepeat') set count(c: number) {
    this.viewContainer.clear();
    for (var i = 0; i < c; i++) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}

// @Directive({ selector: '[biRepeat]' })
// export class RepeatDirective {

//   constructor(private templateRef: TemplateRef<any>,
//     private viewContainer: ViewContainerRef) { }

//   @Input('biRepeat') set count(c: number) {
//     this.viewContainer.clear();
//     for (var i = 0; i < c; i++) {
//       this.viewContainer.createEmbeddedView(this.templateRef);
//     }
//   }
// }
